"use strict";
// const { Model, DataTypes } = require("sequelize");

const bcrypt = require("bcrypt");

const MyModel = require('./crud/user')

const secret = process.env.SECRET || bcrypt.genSaltSync(10)

module.exports = (sequelize) => {

	const { Users } = sequelize.models

	Users.prototype.token = () => jwt.sign({ "id": this.id, "login": this.login, dat: new Date() }, secret)

	return MyModel;
};
