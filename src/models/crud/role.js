"use strict";
const { Model, DataTypes } = require("sequelize");
const bcrypt = require("bcrypt");

module.exports = (sequelize) => {
	class MyModel extends Model {
		/**
		 * Helper method for defining associations.
		 * This method is not a part of Sequelize lifecycle.
		 * The `models/index` file will call this method automatically.
		 */
		static associate(models) {
			// define association here
			models.Roles.belongsToMany(models.Permissions, {through: 'role_permission', onDelete: 'cascade'})
			models.Permissions.belongsToMany(models.Roles, {through: 'role_permission', onDelete: 'cascade'})
		}
	}

	MyModel.init(
		{
			name: {type: DataTypes.STRING, allowNull: false }
		},
		{
			sequelize,
			modelName: "Roles",
			indexes: [
				{ unique: true, fields: ["name"] }
			],
			defaultScope: {
				attributes: { exclude: [] },
			},
			scopes: {
				withPassword: { attributes: {} }
			},
			hooks:{
				
			}
		}
	);

	return MyModel;
};
