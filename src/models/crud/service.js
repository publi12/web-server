"use strict";
const { Model, DataTypes } = require("sequelize");
const bcrypt = require("bcrypt");

module.exports = (sequelize) => {
	class MyModel extends Model {
		/**
		 * Helper method for defining associations.
		 * This method is not a part of Sequelize lifecycle.
		 * The `models/index` file will call this method automatically.
		 */
		static associate(models) {
			// define association here
			models.Users.hasOne(models.Services, {onDelete: 'cascade'})
			models.Services.belongsTo(models.Users, {onDelete: 'cascade'})
			models.Services.belongsToMany(models.Users, {through: 'service_granter', onDelete: 'cascade'})
			models.Users.belongsToMany(models.Services, {through: 'service_granter', onDelete: 'cascade'})
		}
	}

	MyModel.init(
		{
			tags: {type: DataTypes.JSON },
			name: {type: DataTypes.STRING, allowNull: false }
		},
		{
			sequelize,
			modelName: "Services",
			indexes: [
				{ unique: true, fields: ["name"] }
			],
			defaultScope: {
				attributes: { exclude: [] },
			},
			scopes: {
				withPassword: { attributes: {} }
			},
			hooks:{
				
			}
		}
	);

	return MyModel;
};
