"use strict";
const { Model, DataTypes } = require("sequelize");
const bcrypt = require("bcrypt");

module.exports = (sequelize) => {
	class MyModel extends Model {
		/**
		 * Helper method for defining associations.
		 * This method is not a part of Sequelize lifecycle.
		 * The `models/index` file will call this method automatically.
		 */
		static associate(models) {
			// define association here
			models.Services.hasOne(models.Permissions, {onDelete: 'cascade'})
			models.Permissions.belongsTo(models.Services, {onDelete: 'cascade'})
		}
	}

	MyModel.init(
		{
			path: {type: DataTypes.STRING }
		},
		{
			sequelize,
			modelName: "Permissions",
			indexes: [
				
			],
			defaultScope: {
				attributes: { exclude: [] },
			},
			scopes: {
				withPassword: { attributes: {} }
			},
			hooks:{
				
			}
		}
	);

	return MyModel;
};
