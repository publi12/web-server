"use strict";
const { Model, DataTypes } = require("sequelize");
const bcrypt = require("bcrypt");

module.exports = (sequelize) => {
	class MyModel extends Model {
		/**
		 * Helper method for defining associations.
		 * This method is not a part of Sequelize lifecycle.
		 * The `models/index` file will call this method automatically.
		 */
		static associate(models) {
			// define association here
			models.Users.belongsToMany(models.Roles, {through: 'user_role', onDelete: 'cascade'})
			models.Roles.belongsToMany(models.Users, {through: 'user_role', onDelete: 'cascade'})
		}

		
        static async login(login, password){
            const where = sequelize.where(sequelize.fn('LOWER', sequelize.col('login')), login.toLowerCase())
            const user = await this.findOne({ where });
            return (user && bcrypt.compareSync(password, user.getDataValue('password'))) ? user : null;
        }
	}

	MyModel.init(
		{
			login: {type: DataTypes.STRING, allowNull: false },
			password: {type: DataTypes.STRING, get(){}, set(value) {this.setDataValue('password', bcrypt.hashSync(value, bcrypt.genSaltSync(10)) )} }
		},
		{
			sequelize,
			modelName: "Users",
			indexes: [
				{ unique: true, fields: ["login"] }
			],
			defaultScope: {
				attributes: { exclude: [] },
			},
			scopes: {
				withPassword: { attributes: {} }
			},
			hooks:{
				
			}
		}
	);

	return MyModel;
};
