"use strict";
const { Model, DataTypes } = require("sequelize");
const bcrypt = require("bcrypt");

module.exports = (sequelize) => {
	class MyModel extends Model {
		/**
		 * Helper method for defining associations.
		 * This method is not a part of Sequelize lifecycle.
		 * The `models/index` file will call this method automatically.
		 */
		static associate(models) {
			// define association here
			models.Users.hasOne(models.Sessions, {onDelete: 'cascade'})
			models.Sessions.belongsTo(models.Users, {onDelete: 'cascade'})
		}
	}

	MyModel.init(
		{
			token: {type: DataTypes.STRING },
			userAgent: {type: DataTypes.STRING },
			userAddress: {type: DataTypes.STRING }
		},
		{
			sequelize,
			modelName: "Sessions",
			indexes: [
				
			],
			defaultScope: {
				attributes: { exclude: [] },
			},
			scopes: {
				withPassword: { attributes: {} }
			},
			hooks:{
				
			}
		}
	);

	return MyModel;
};
