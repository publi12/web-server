"use strict";
const { Model, DataTypes } = require("sequelize");
const bcrypt = require("bcrypt");

module.exports = (sequelize) => {
	class MyModel extends Model {
		/**
		 * Helper method for defining associations.
		 * This method is not a part of Sequelize lifecycle.
		 * The `models/index` file will call this method automatically.
		 */
		static associate(models) {
			// define association here
			models.Users.hasOne(models.Tovars, {onDelete: 'cascade'})
			models.Tovars.belongsTo(models.Users, {onDelete: 'cascade'})
		}
	}

	MyModel.init(
		{
			name: {type: DataTypes.STRING },
			price: {type: DataTypes.FLOAT }
		},
		{
			sequelize,
			modelName: "Tovars",
			indexes: [
				
			],
			defaultScope: {
				attributes: { exclude: [] },
			},
			scopes: {
				withPassword: { attributes: {} }
			},
			hooks:{
				
			}
		}
	);

	return MyModel;
};
