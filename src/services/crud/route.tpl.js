const { connect } = require('../db')
const { handleExpress } = require('../../utils/handle-express')

const router = require('express').Router()

//--- Auto generated crud routes 
router.get('/', (req, res) => {
    handleExpress(res, async () => {
        const db = await connect()
        const model = db.models['$models']
        return model.findAll()
    })
})

router.post('/', (req, res) => {
    handleExpress(res, async () => {
        const db = await connect()
        const model = db.models['$models']
        return model.create(req.body)
    })
})

router.get('/:id', (req, res) => {
    handleExpress(res, async () => {
        const db = await connect()
        const model = db.models['$models']
        return model.findByPk(req.params.id)
    })
})

router.put('/:id', (req, res) => {
    handleExpress(res, async () => {
        const db = await connect()
        const model = db.models['$models']
        return model.upsert({ id: req.params.id, ...req.body })
    })
})

router.delete('/:id', (req, res) => {
    handleExpress(res, async () => {
        const db = await connect()
        const model = db.models['$models']
        return model.delete({ where: { id: req.params.id } })
    })
})



module.exports = router