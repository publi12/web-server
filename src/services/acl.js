const { Router } = require("express");
const { connect } = require("./db");
const { cap } = require("../utils/webserver/cap");

const ACL_URL = 'http://localhost/api/'
const SERVICE_ID = process.env.SERVICE_ID

const aclAllowAll = ['users-register']

let perms = []

async function registerAclPermissions(permissions) {
    perms = permissions
}

const role = {
    name: "role1",
    // perms: [
    //     'u.get/api/services/{id}',
    //     'u.get/api/services/',
    //     'post/api/services/',
    //     'post/api/services/register/{key}',
    // ]

    perms: [
        { method: 'get', path: ['services'], flags: '' },
        // { method: 'get', path: ['services'], flags: 'u' },
        { method: 'get', path: ['services', '{id}'], flags: 'u' },
        { method: 'post', path: ['services'], flags: '' },
        { method: 'post', path: ['services', 'register', '{key}'], flags: 'u' },

        // ['u', 'get/api/services/{id}'],
        // ['u', 'get/api/services/'],
        // ['r', 'post/api/services/'],
        // ['', 'post/api/services/register/{key}'],
    ]
}

// const crudRouter = Router()

// const handleSql = (res, promise) => {
//     promise
//         .then(x => {
//             if (!x)
//                 res.sendStatus(404)
//             else
//                 res.send(isFinite(x) ? [x] : x)
//         })
//         .catch(ex => {
//             const pm = ex.parent && ex.parent.sqlMessage
//             const em = ex.errors && ex.errors.map(e => e.message).join('; ')
//             res.status(400).send(pm || em)
//         })
// }

// crudRouter.use('/:model', async (req, res, next) => {
//     // !!! ACL
//     const db = await connect()
//     const model = db.models[cap(req.params.model)]
//     req.db = db
//     req.model = model
//     next()
// })

// crudRouter.get('/:model', async (req, res) => {
//     console.log(req.query)
//     handleSql(res, req.model.findAll())
// })

// crudRouter.post('/:model', async (req, res) => {
//     handleSql(res, req.model.create(req.body))
// })

// crudRouter.get('/:model/:id', async (req, res) => {
//     handleSql(res, req.model.findByPk(req.params.id))
// })

// crudRouter.put('/:model/:id', async (req, res) => {
//     handleSql(res, req.model.update(req.body, { where: { id: req.params.id } }))
// })

// crudRouter.delete('/:model/:id', async (req, res) => {
//     handleSql(res, req.model.destroy({ where: { id: req.params.id } }))
// })


// return crudRouter

async function expressCrud(req, res, next) {

    // console.log(ACL_URL + `services/${SERVICE_ID}/acl?m=${req.method}&p=${req.originalUrl}`, { headers: req.headers })
    const key = `${req.method}/${req.originalUrl}`.replace('//', '/').toLowerCase()

    const [url, query] = req.url.split('?')
    const [model, id, submodel, sid] = url.slice(1).split('/')

    console.log(key, req.url) //model/{id}/{submodel}/{sid}
    console.log([model, id, submodel, sid])

    // console.log(key)

    const pkeys = perms.map(x => `${x[0]}/${x[1]}`.replace('//', '/').toLowerCase())
    // console.log(pkeys)

    // --- Все варианты действий
    // Если правило на owner, то проверим его
    // GET /model
    // GET /model/:id
    // POST /model/
    // PUT /model/:id
    // DELETE /model/:id

    // POST /model/:id/action { body }

    // GET /model/:id/submodel - detach
    // PUT /model/:id/submodel/:sid - attach
    // DELETE /model/:id/submodel/:sid - detach
    // DELETE /model/:id/submodel - detach all


    // role.perms = role.perms.map(x => [x[0], x[1].split('/').filter(x => x)])

    function findPerm(perm, list) {
        for (let rec of list) {
            const p = rec[1]
            if (p.length != perm.length) continue;

            // console.log(p, perm)

            const aa = perm.filter((x, i) => !p[i].includes('{') && perm[i] != p[i])

            // Правило совпало
            if (!aa.length) {
                return rec
            }
        }
    }

    function compareRoute(tpl, route) {
        route = route.split('?')[0]
        const tp = tpl.split('/').filter(x => x)
        const rp = route.split('/').filter(x => x)

        // console.log({tp, rp})

        if (tp.length != rp.length) return false;

        const aa = tp.filter((x, i) => !tp[i].includes('{') && tp[i] != rp[i])

        console.log(role.perms)

        // Правило совпало
        if (!aa.length) {
            console.log(tpl, route, aa.length)
            const ruleIsUser = 1
            const ruleIsOrg = 1

            // const db = await connect()
            // db.models['']

            // Если правило на owner, то проверим его
            // GET /model
            // {where}
            // GET /model/:id
            // {where}
            // PUT /model/:id
            // {where}
            // DELETE /model/:id
            // {where}

            // POST /model/:id/action { body }
            // {where}

            // GET /model/:id/submodel - detach
            // {where: sid}
            // PUT /model/:id/submodel/:sid - attach
            // {where: sid}
            // DELETE /model/:id/submodel/:sid - detach
            // {where: sid}
            // DELETE /model/:id/submodel - detach all
            // {where: sid}


            // Если правило на org, то проверим его
        }


        // service permissions
        // delete service/23/premissions
        // get service/23/premissions owner, org
        // get service/23/nodes/
        // post service/23/permission/

        // model.ofUser()
        // model.ofOrg()
    }

    const rr = key.split('?')[0].split('/').filter(x => x)

    // pkeys.map(x => compareRoute(x, key))
    // console.log(rr, role.perms)
    const perm = findPerm(rr, role.perms)
    console.log({ perm })

    if (!perm) return res.sendStatus(403)

    // if(perm[0].includes('u')){
    //     // Filter model data
    //     return res.sendStatus(403)
    // }

    // if(perm[0].includes('o')){
    //     // Filter model data
    //     return res.sendStatus(403)
    // }

    // if(perm[0].includes('r')){
    //     // Filter model data
    //     return res.sendStatus(403)
    // }

    // fetch
    // if (req.originalUrl == `/api/services/2`) return res.sendStatus(401)
    next()
}

function expressAcl(req, res, next) {
    // console.log(req.baseUrl)
    // console.log(ACL_URL + `services/${SERVICE_ID}/acl?m=${req.method}&p=${req.originalUrl}`, { headers: req.headers })
    const key = `${req.method}/${req.originalUrl}`.replace('//', '/').toLowerCase()

    console.log(key, req.url)

    // console.log(key)

    const pkeys = perms.map(x => `${x[0]}/${x[1]}`.replace('//', '/').toLowerCase())
    // console.log(pkeys)

    // --- Все варианты действий
    // Если правило на owner, то проверим его
    // GET /model
    // GET /model/:id
    // POST /model/
    // PUT /model/:id
    // DELETE /model/:id

    // POST /model/:id/action { body }

    // GET /model/:id/submodel - detach
    // PUT /model/:id/submodel/:sid - attach
    // DELETE /model/:id/submodel/:sid - detach
    // DELETE /model/:id/submodel - detach all


    const role = {
        name: "role1",
        // perms: [
        //     'u.get/api/services/{id}',
        //     'u.get/api/services/',
        //     'post/api/services/',
        //     'post/api/services/register/{key}',
        // ]

        perms: [
            ['u', 'get/api/services/{id}'],
            ['u', 'get/api/services/'],
            ['r', 'post/api/services/'],
            ['', 'post/api/services/register/{key}'],
        ]
    }

    role.perms = role.perms.map(x => [x[0], x[1].split('/').filter(x => x)])

    function findPerm(perm, list) {
        for (let rec of list) {
            const p = rec[1]
            if (p.length != perm.length) continue;

            // console.log(p, perm)

            const aa = perm.filter((x, i) => !p[i].includes('{') && perm[i] != p[i])

            // Правило совпало
            if (!aa.length) {
                return rec
            }
        }
    }

    function compareRoute(tpl, route) {
        route = route.split('?')[0]
        const tp = tpl.split('/').filter(x => x)
        const rp = route.split('/').filter(x => x)

        // console.log({tp, rp})

        if (tp.length != rp.length) return false;

        const aa = tp.filter((x, i) => !tp[i].includes('{') && tp[i] != rp[i])

        console.log(role.perms)

        // Правило совпало
        if (!aa.length) {
            console.log(tpl, route, aa.length)
            const ruleIsUser = 1
            const ruleIsOrg = 1

            // const db = await connect()
            // db.models['']

            // Если правило на owner, то проверим его
            // GET /model
            // {where}
            // GET /model/:id
            // {where}
            // PUT /model/:id
            // {where}
            // DELETE /model/:id
            // {where}

            // POST /model/:id/action { body }
            // {where}

            // GET /model/:id/submodel - detach
            // {where: sid}
            // PUT /model/:id/submodel/:sid - attach
            // {where: sid}
            // DELETE /model/:id/submodel/:sid - detach
            // {where: sid}
            // DELETE /model/:id/submodel - detach all
            // {where: sid}


            // Если правило на org, то проверим его
        }


        // service permissions
        // delete service/23/premissions
        // get service/23/premissions owner, org
        // get service/23/nodes/
        // post service/23/permission/

        // model.ofUser()
        // model.ofOrg()
    }

    const rr = key.split('?')[0].split('/').filter(x => x)

    // pkeys.map(x => compareRoute(x, key))
    // console.log(rr, role.perms)
    const perm = findPerm(rr, role.perms)
    console.log({ perm })

    if (!perm) return res.sendStatus(403)

    // if(perm[0].includes('u')){
    //     // Filter model data
    //     return res.sendStatus(403)
    // }

    // if(perm[0].includes('o')){
    //     // Filter model data
    //     return res.sendStatus(403)
    // }

    // if(perm[0].includes('r')){
    //     // Filter model data
    //     return res.sendStatus(403)
    // }

    // fetch
    // if (req.originalUrl == `/api/services/2`) return res.sendStatus(401)
    next()
}

function acl(action) {
    return async (req, res, next) => {

        if (aclAllowAll.includes(action)) return next();

        console.log(ACL_URL + `/services/${SERVICE_ID}/allowed/${action}`, { headers: req.headers })
        try {
            const aa = await fetch(ACL_URL + `?a=${action}`, { headers: req.headers })
            // console.log(aa)
            if (aa.status > 299) res.sendStatus(aa.status)
            else next()
        }
        catch {
            res.sendStatus(400)
        }
    }
}

async function auth(req, res, next) {
    const auth = req.query.utoken || req.headers.authorization
    if (!auth) return res.sendStatus(401)

    const db = await connect()
    const { Sessions, Users } = db.models

    let user
    const [type, value] = auth.split(' ')
    // console.log({ type, value })
    if (!value || type == 'Bearer') {
        // Find session
        const session = await Sessions.findOne({ where: { token: value || type }, include: 'User' })
        if (!session) return res.sendStatus(401)
        user = session.User
        req.session = session
    }
    else if (type == 'Basic') {
        const [login, password] = Buffer.from(value, 'base64').toString().split(':')
        // Find user

        const loginData = await Users.login(login, password)
        if (!loginData) return res.sendStatus(401)
        //  eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwiZW1haWwiOiJhbnkiLCJpYXQiOjE3MTE0NTY3MDV9.BOj6dQrwRv9Z_Uw0dFQcj10czMKLZmIQhtveFrJz9sU
        user = loginData.user
    }
    else {
        return res.sendStatus(401)
    }

    req.user = user
    next()
}

function buildUserToken(user) {
    return jwt.sign({ "id": user.id, "login": user.login }, process.env.SECRET || 'secret');
}

module.exports = { acl, auth, registerAclPermissions, expressAcl, expressCrud, buildUserToken }