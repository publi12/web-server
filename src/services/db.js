const { Sequelize } = require('sequelize');
const { QueryTypes } = require('sequelize');
const fs = require("fs");
const path = require("path");
const walkdir = require('walkdir');
const basename = path.basename(__filename);

const dbUri = process.env.DB_URI || ''
if (!process.env.DB_URI) throw 'DB_URI not defined, Like sqlite:///data/base.sqlite'

// const dbPrefix = process.env.DB_PREFIX || ''
const dbEngine = dbUri.slice(0, dbUri.indexOf(':'))
// const dbUriPrefix = dbUri.slice(0, dbUri.lastIndexOf('/') + 1)
// const dbUriSuffix = dbUri.slice(dbUri.lastIndexOf('/'))
// const dbExt = dbUriSuffix.includes('.') ? dbUriSuffix.slice(dbUriSuffix.lastIndexOf('.')) : ''
// const dbName = (dbUriSuffix.startsWith('/') ? dbUriSuffix.slice(1) : dbUriSuffix).slice(0, -dbExt.length)

let db = null

function loadAllModels(sequelize, dir) {
    if (!fs.existsSync(dir)) return;

    walkdir.sync(dir, file => {
        if (path.basename(file).startsWith('_')) return; 
        if (!file.endsWith('.js')) return;

        // console.log(file)
        require(file)(sequelize)
    })

    Object.keys(sequelize.models).forEach((modelName) => {
        if (sequelize.models[modelName].associate) {
            sequelize.models[modelName].associate(sequelize.models);
        }
    });
}

async function connect(sync = false, modelsPath = 'models') {
    if (!db || sync) {
        db = new Sequelize(dbUri, {
            logging: process.env.DB_LOGGING,
            define: {
                // timestamps: false
            }
        })
        // if (isSqlite) 
        if (dbEngine == 'sqlite')
            try { await db.query('PRAGMA journal_mode=OFF') } catch { };

        loadAllModels(db, __dirname + '/../' + modelsPath)
        if (sync){
            console.log("Synchronizing DB with models...")
            await db.sync({ alter: true })
        }
    }

    return db
}

module.exports = {
    connect,
}