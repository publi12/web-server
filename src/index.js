process.env.TZ = 'Europe/London'
require('dotenv').config()

// const serve = require('./utils/webserver');
const { createWebServer } = require('./utils/webserver')
const { connect } = require('./services/db');
const express = require('express');

const { updateCrudModels, readConfig } = require('./utils/webserver/crud-models');
// const { install } = require('./utils/webserver/install');
const { PERM_READ } = require('./const/permissions');
const { registerAclPermissions } = require('./services/acl');
const { install } = require('./utils/webserver/install');

console.log(PERM_READ)

// useCrud - (modelsPath, crudUrl -> crudRouter) 
// useAcl - 

const roles = [
    {
        name: "manager",
        perms: ['GET services', 'services/*']
    }
]

async function start() {

    updateCrudModels()
    // crudUpdateRoutes()

    // const db = await connect(true)
    await install()

    // for (let i = 0; i < 100; i++)
    // await db.query(`ALTER TABLE Roles DROP INDEX IF EXISTS name_${i}`)

    // await connect(false)
    const { app, perms } = await serve()
    await registerAclPermissions(perms)
    // console.log(perms)

    // await registerAclPermissions(perms)

    //     console.log(data.paths)
    //     console.log(perms)
    // }
}

async function start2() {
    const modelsPath = __dirname + '/models/crud.yml'
    const routerPath = __dirname + '/routes/index.js'

    const server = createWebServer(routerPath, modelsPath)

    const db2 = await connect()
    const { Users } = db2.models

    // const uu = (await Users.findByPk(1)).toJSON()// login('12345', '12345')
    // const uu = await Users.login('12345', '12345')
    // console.log(uu)


    // const crudModels = readConfig(__dirname + '/models/crud.yml')

    // await install(__dirname + '/models/crud.yml')

    // await serve(__dirname + '/routes/index.js', )

    return
    // updateCrudModels(__dirname + '/models/crud.yml')

    // const db = await connect(true)

    // const { Users } = db.models
    // await Users.create({login: '12345', password: '12345'})
    // const u = await Users.scope('withPassword').findByPk(1)
    // console.log(u)
    // u.login = '123'
    // u.password = '123'
    // await u.save()
    // u.password = 100
    // // console.log(u)

    // const uu = await Users.login('12345', '12345')
    // console.log(uu)
}

start2()

// start()

const functi = x => { }

module.exports = {
    install,
    connect
}