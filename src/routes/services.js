const { default: axios } = require('axios')
// const { v4: uuidv4 } = require('uuid');
const { connect } = require('../services/db')
const { handleExpress } = require('../utils/handle-express')

const router = require('express').Router()

const STATIC_ACL = [
    { sid: 0, uid: 0, a: 'users-register' }
]

router.get('/', (req, res) => {
    // #swagger.tags = ['Services']
    handleExpress(res, async () => {
        // throw 401
        const db = await connect()
        const { Services } = db.models
        return Services.findAll()
    })
})

router.post('/', (req, res) => {
    // #swagger.tags = ['Services']
    const { name } = req.body
    handleExpress(res, async () => {
        const db = await connect()
        const { Services } = db.models

        const service = await Services.create({
            name,
            // token: uuidv4().replace(/-/g, '')
        })
        // await service.updateRights()
        return service
    })
})

router.post('/register/:key', (req, res) => {
    // #swagger.tags = ['Services']
    const { name } = req.body
    handleExpress(res, async () => {
        const db = await connect()
        const { Services } = db.models

        const service = await Services.create({
            name,
            // token: uuidv4().replace(/-/g, '')
        })
        // await service.updateRights()
        return service
    })
})

router.get('/:id', (req, res) => {
    // #swagger.tags = ['Services']
    // TODO: ACL
    handleExpress(res, async () => {
        const db = await connect()
        const { Services } = db.models
        return Services.findByPk(req.params.id)
    })
})

// router.put('/:id', (req, res) => {
//     // #swagger.tags = ['Services']
//     res.send('asdasd')
// })

router.delete('/:id', async (req, res) => {
    // #swagger.tags = ['Services']
    // TODO: ACL
    handleExpress(res, async () => {
        const db = await connect()
        const { Services } = db.models
        return Services.destroy({ where: { id: req.params.id } })
    })
})

router.get('/:id/permissions/', (req, res) => {
    // #swagger.tags = ['Services']
    handleExpress(res, async () => {
        const db = await connect()
        const { Permissions } = db.models
        return Permissions.findAll({ where: { serviceId: req.params.id } })
    })
})

router.post('/:id/permissions/', (req, res) => {
    // #swagger.tags = ['Services']
    handleExpress(res, async () => {
        const db = await connect()
        // const { Services } = db.models
        const { Permissions } = db.models
        // const service =  Services.findByPk(req.params.id)
        const { name, title } = req.body
        return Permissions.create({ ServiceId: req.params.id, name, title })
        // service.actions

        // return service.actions
    })
})

router.get('/:id/permissions/:actions', (req, res) => {
    // #swagger.tags = ['Services']
    handleExpress(res, async () => {
        const db = await connect()
        // const { Services } = db.models
        const { Permissions, Sessions } = db.models
        let { id, actions } = req.params
        actions = actions.split(',').map(x => x.trim())

        // Get user from session

        // Get all roles of user

        // Get all permissions for service of roles

        // Compare roles

        const perms = await Permissions.findAll({ where: { ServiceId: req.params.id, name: actions} }) 

        // if (perms != actions.length) throw 403

        return perms

        // TODO: Get user
        // const session = sessions.restore()
        // sessionStorage
        console.log(req.headers)

        const userId = 0
        // Check static ACL
        const filter = x => (!x.sid || x.sid == id)
            && (!x.uid || x.uid == userId)
            && (x.a == "*" || actions.includes(x.a))
        if (STATIC_ACL.filter(filter).length) {
            return 0
        }

        const where = { serviceId: req.params.id, name: actions }
        const acts = await Actions.findAll({ where })

        console.log(id, actions, acts)
        // const service =  Services.findByPk(req.params.id)
        // const {name, title} = req.body
        // return Actions.create({serviceId: req.params.id, name, title })
        // service.actions
        throw 403
        // return service.actions
    })
})



// router.post('/:id/updateRights', (req, res) => {
//     // #swagger.tags = ['Services']
//     handleExpress(res, async () => {
//         const db = await connect()
//         const { Services, ServiceRights } = db.models

//         const ServiceId = req.params.id

//         const service = await Services.findByPk(ServiceId)//, {include: [ServiceRights]})



//         // console.log(await service.addServiceRight({name: 'name3'}))

//         const rights = await axios.get(service.rightsUrl).then(x => x.data)
//         // console.log(await service.createServiceRight(rights[0]))

//         await ServiceRights.upsert({ ...rights[0], ServiceId })

//         // await ServiceRights.create({...rights[0], ServiceId: 1})
//         // .map(x => ({...x, ServiceId: req.params.id})))
//         // console.log(await service.getServiceRights())

//         // console.log(service)
//         // return 1
//         // // console.log(await service.setServiceRights(rights))
//         // console.log(await ServiceRights.bulkCreate(rights.map(x => ({...x, ServiceId: req.params.id}))))
//         // console.log(rights)

//         return service
//     })
// })


module.exports = router