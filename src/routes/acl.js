const { default: axios } = require('axios')
const { connect } = require('../services/db')
const { handleExpress } = require('../utils/handle-express')

const router = require('express').Router()

router.get('/', (req, res) => {
    // #swagger.tags = ['Acl']
    handleExpress(res, async () => {
        const db = await connect()
        const Users = require('../models/user')(db)
        return Users.findAll()
    })
})

router.get('/allowed', async (req, res) => {
    // #swagger.tags = ['Acl']
    // Take params from query or headers
    const auth = req.query.utoken || req.headers.authorization
    if (!auth) return res.sendStatus(401)

    const db = await connect()
    const { Sessions, Users, Actions, Roles } = db.models

    let userId
    const [type, value] = auth.split(' ')
    console.log({ type, value })
    if (type == 'Bearer') {
        // Find session
        const session = await Sessions.findByPk(value)
        // Find user
        userId = session.userId
    }
    else if (type == 'Basic') {
        const [login, password] = Buffer.from(value, 'base64').toString().split(':')
        // Find user
        console.log([value, login, password])
        if (login == process.env.ADMIN_USER && password == process.env.ADMIN_PASS) {
            console.log('SUperadmin')
            return res.sendStatus(200)
        }

        const loginData = await Users.login(login, password)
        if (!loginData) return res.sendStatus(401)

        userId = loginData.user.id
    }
    else {
        return res.sendStatus(401)
    }

    const serviceId = req.query.serviceId || req.headers.serviceId
    const action = req.query.action || req.headers.action

    const act = await Actions.findAll({where: {Service}})


    const user = await Users.findByPk(userId)

    const actions = await user.getRoles({ include: [Actions] })
    console.log(actions)

    // Find allowance
    // Send answer
    // return res.send(user)

    return res.send(`` + userId)


    console.log({ stoken, action, auth })
    handleExpress(res, async () => {
        console.log(res.headersSent)
        res.sendStatus(403)
        console.log(res.headersSent)
        const db = await connect()
        const Users = require('../models/user')(db)
        return Users.findAll()
    })
})

router.post('/', async (req, res) => {
    // #swagger.tags = ['Acl']
    const aa = await axios.get(ACL_URL + '?stoken=0&utoken=123456787786&a=users.write')

    res.send(aa)
})

router.get('/:id', (req, res) => {
    // #swagger.tags = ['Acl']
    handleExpress(res, async () => {
        const db = await connect()
        const User = require('../models/user')(db)
        return await User.findAll()
    })
})

router.put('/:id', (req, res) => {
    // #swagger.tags = ['Acl']
    res.send('asdasd')
})

router.delete('/:id', (req, res) => {
    // #swagger.tags = ['Acl']
    res.send('asdasd')
})


module.exports = router