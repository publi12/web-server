const { connect } = require('../services/db')
const { handleExpress } = require('../utils/handle-express')
const { acl, auth } = require('../services/acl')

const router = require('express').Router()

router.get('/me', auth, (req, res) => {
    // #swagger.tags = ['Users']
    handleExpress(res, async () => {
        return req.user

    })
})

router.get('/', (req, res) => {
    // #swagger.tags = ['Users']
    handleExpress(res, async () => {
        const db = await connect()
        const { Users } = db.models
        return Users.findAll({ attributes: ["id", "login", "createdAt"] })
    })
})

router.post('/', acl('users-write'), (req, res) => {
    // #swagger.tags = ['Users']
    const { login, password, email } = req.body

    handleExpress(res, async () => {
        const db = await connect()
        const { Users } = db.models
        return Users.createNew(login, password, { email }).then(x => x.id)
    })
})

router.post('/register', acl('users-register'), (req, res) => {
    // #swagger.tags = ['Users']
    const { login, password, email } = req.body

    console.log(req.body)

    handleExpress(res, async () => {
        const db = await connect()
        const { Users } = db.models
        return Users.createNew(login, password, { email }).then(x => ({ id: x.id, login: x.login }))
    })
})

router.get('/:id', (req, res) => {
    // #swagger.tags = ['Users']
    handleExpress(res, async () => {
        const db = await connect()
        const { Users } = db.models
        return Users.findByPk(req.params.id, { attributes: ["id", "login", "createdAt"] })
    })
})

// router.put('/:id', (req, res) => {
//     // #swagger.tags = ['Users']
//     res.send('asdasd')
// })

router.delete('/:id', acl('users-write'), (req, res) => {
    // #swagger.tags = ['Users']
    // TODO: ACL users-write

    res.send('asdasd')
})

router.get('/:id/roles', (req, res) => {
    // #swagger.tags = ['Users']
    handleExpress(res, async () => {
        const db = await connect()
        const { Users } = db.models
        return Users.findByPk(req.params.id, { include: 'Roles' }).then(x => x.Roles)
    })
})

router.post('/:id/roles', (req, res) => {
    // #swagger.tags = ['Users']
    handleExpress(res, async () => {
        const db = await connect()
        const { Users, Roles } = db.models
        const user = await Users.findByPk(req.params.id)
        return user.addRole(await Roles.findByPk(req.body.id))
    })
})

router.delete('/:id/roles', (req, res) => {
    // #swagger.tags = ['Users']
    res.send('asdasd')
})

router.delete('/:id/roles/:rid', (req, res) => {
    // #swagger.tags = ['Users']
    res.send('asdasd')
})




module.exports = router