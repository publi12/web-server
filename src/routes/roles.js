const { default: axios } = require('axios')
// const { v4: uuidv4 } = require('uuid');
const { connect } = require('../services/db')
const { handleExpress } = require('../utils/handle-express')

const router = require('express').Router()

router.get('/', (req, res) => {
    // #swagger.tags = ['Roles']
    handleExpress(res, async () => {
        const db = await connect()
        const { Roles } = db.models
        return Roles.findAll()
    })
})

router.post('/', (req, res) => {
    // #swagger.tags = ['Roles']
    const { name } = req.body
    handleExpress(res, async () => {
        const db = await connect()
        const { Roles } = db.models

        return Roles.create({ name })
    })
})

router.get('/:id', (req, res) => {
    // #swagger.tags = ['Roles']
    // TODO: ACL
    handleExpress(res, async () => {
        const db = await connect()
        const { Roles } = db.models
        return Roles.findByPk(req.params.id)
    })
})

// router.put('/:id', (req, res) => {
//     // #swagger.tags = ['Roles']
//     res.send('asdasd')
// })

router.delete('/:id', async (req, res) => {
    // #swagger.tags = ['Roles']
    // TODO: ACL
    handleExpress(res, async () => {
        const db = await connect()
        const { Roles } = db.models
        return Roles.destroy({ where: { id: req.params.id } })
    })
})

router.get('/:id/permissions/', (req, res) => {
    // #swagger.tags = ['Roles']
    handleExpress(res, async () => {
        const db = await connect()
        const { Roles, Permissions } = db.models

        const role = await Roles.findByPk(req.params.id, { include: Permissions }).then(x => x.Permissions)
        return role
        // const Permissions = await RolePermissions.findAll({ where: { roleId: req.params.id }, include: [Permissions] })

        // return Permissions
    })
})

router.post('/:id/permissions/', (req, res) => {
    // #swagger.tags = ['Roles']
    handleExpress(res, async () => {
        const db = await connect()
        const { Roles, Permissions } = db.models
        const role = await Roles.findByPk(req.params.id)//, {include: Permissions})
        console.log(role)
        return role.addPermission(await Permissions.findByPk(req.body.id))
        // return await role.addPermission({id: req.body.id})
        // const { RolePermissions } = db.models
        // return RolePermissions.create({ roleId: req.params.id, PermissionId: req.body.id })

    })
})

router.delete('/:id/permissions/:aid', (req, res) => {
    // #swagger.tags = ['Roles']
    handleExpress(res, async () => {
        const db = await connect()
        // const { Roles } = db.models
        const { Permissions } = db.models
        // const service =  Roles.findByPk(req.params.id)
        const { name, title } = req.body
        return Permissions.create({ serviceId: req.params.id, name, title })
    })
})

router.delete('/:id/permissions/', (req, res) => {
    // #swagger.tags = ['Roles']
    handleExpress(res, async () => {
        const db = await connect()
        // const { Roles } = db.models
        const { Permissions } = db.models
        // const service =  Roles.findByPk(req.params.id)
        const { name, title } = req.body
        return Permissions.create({ serviceId: req.params.id, name, title })
    })
})


module.exports = router