// const { acl } = require('../../services/acl')
const { readConfig, toTablename } = require('../../services/crud/crud-models')
const { connect } = require('../../services/db')
const { handleExpress } = require('../../utils/handle-express')

const router = require('express').Router()

//--- Auto generated crud routes
router.get('/', (req, res) => {
    // #swagger.tags = ['Crud']
    handleExpress(res, async () => {
        // const db = await connect()
        // const model = db.models[toTablename(req.params.model)]
        return req.model.findAll()
    })
})

router.post('/', (req, res) => {
    // #swagger.tags = ['Crud']
    // #swagger.parameters['fields'] = { description: 'Some description...', in: 'body' }
    handleExpress(res, req.model.create(req.body))
})

router.get('/:id', (req, res) => {
    // #swagger.tags = ['Crud']
    handleExpress(res, req.model.findByPk(req.params.id))
})

router.put('/:id', (req, res) => {
    // #swagger.tags = ['Crud']
    // #swagger.parameters['fields'] = { description: 'Some description...', in: 'body' }
    handleExpress(res, async () => {
        let item = await req.model.findByPk(req.params.id)
        if (!item) item = req.model.create(req.body)
        else {
            await req.model.update(req.body, { where: { id: req.params.id } })
            item = await req.model.findByPk(req.params.id)
        }
        return item
    })
})

router.patch('/:id', (req, res) => {
    // #swagger.tags = ['Crud']
    // #swagger.parameters['fields'] = { description: 'Some description...', in: 'body' }
    handleExpress(res, req.model.upsert(req.body))
})

router.delete('/:id', (req, res) => {
    // #swagger.tags = ['Crud']
    handleExpress(res, req.model.destroy({ where: { id: req.params.id } }))
})

router.get('/:id/:submodel/', (req, res) => {
    // #swagger.tags = ['Crud']
    handleExpress(res, async () => {
        return 'empty'
        // const db = await connect()
        // const model = db.models[req.params.model]
        // return model.findAll()
    })
})

router.post('/:id/:submodel/', (req, res) => {
    // #swagger.tags = ['Crud']
    handleExpress(res, async () => {
        return 'empty'
    })
})

module.exports = router