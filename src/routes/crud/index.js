const { acl } = require('../../services/acl')
const { toTablename } = require('../../services/crud/crud-models')
const { connect } = require('../../services/db')
const { handleExpress } = require('../../utils/handle-express')

const router = require('express').Router()

// router.use('/:model/', acl('users-write'), (req, res, next) => {
//     // #swagger.tags = ['Crud']
//     handleExpress(res, async () => {
//         const db = await connect()
//         const model = db.models[req.params.model]
//         return model.findAll()
//     })
// })

router.use('/models/', require('./models'))

router.use('/:model/', async (req, res, next) => {
    console.log(req.params, toTablename(req.params.model))

    const db = await connect()
    req.model = db.models[toTablename(req.params.model)]
    next()
}, require('./crud-model'))

module.exports = router