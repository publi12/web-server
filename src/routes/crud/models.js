const router = require('express').Router()
const fs = require('fs')
const path = require('path')

const { readConfig } = require('../../services/crud/crud-models')

// const dirs = fs.readdirSync(__dirname).filter(x => x.endsWith('.js') && x != 'index.js')
// console.log(dirs)

// const models = [
//     {
//         name: 'model1',
//         fields: [
//             { name: 'name', type: 'string' },
//             { name: 'age', type: 'number' },
//             { name: 'model2', type: 'ref1n' },
//         ]
//     }
// ]

// const modelsDir = __dirname // + '/models'

// function focModelsDir(){
//     if (!fs.existsSync(modelsDir)) fs.mkdirSync(modelsDir)
// }

// function readModels(){
//     focModelsDir()
//     const models = fs.readdirSync(modelsDir)
//         .filter(x => x.endsWith('.js') 
//             && !x.endsWith('.tpl.js') 
//             && x != 'index.js'
//             && x != 'models.js').map(x => x.replace('.js', '').slice(0, -1))
//     console.log({models})
//     return models
// }

// function updateCrudRouter(){
//     const models = readModels()

//     let parts = ["const router = require('express').Router()"]
//     parts.push(`router` + `.use('/models', require('./models'))`)
//     parts = [...parts, ...models.map(x => `router` + `.use('/${x}s', require('./${x}s'))`)]
//     parts.push("module.exports = router")

//     fs.writeFileSync(path.join(modelsDir, 'index.js'), parts.join('\r\n'))
// }

// function createModel(name, fields){
//     focModelsDir()
//     const tpl = fs.readFileSync(path.join(modelsDir, 'route.tpl.js')).toString()
//     const out = tpl.replace(/\$model/g, name)
//     console.log(out)
//     console.log(path.join(modelsDir, name))
//     fs.writeFileSync(path.join(modelsDir, name + 's.js'), out)
//     updateCrudRouter()
// }

// function dropModel(name){
//     focModelsDir()
// }


router.get('/', (req, res) => {
    // #swagger.tags = ["Crud-models"]
    // readModels()
    res.send(readConfig())
})

router.post('/', (req, res) => {
    // #swagger.tags = ["Crud-models"]
    createModel(req.body.name, req.body.fields)
    res.send(req.body)
})

router.get('/:id', (req, res) => {
    // #swagger.tags = ["Crud-models"]
    res.send(req.body)
})

router.put('/:id', (req, res) => {
    // #swagger.tags = ["Crud-models"]
    res.send(req.body)
})

router.delete('/:id', (req, res) => {
    // #swagger.tags = ["Crud-models"]
    res.send(req.body)
})



module.exports = router