const { connect } = require('../services/db')
const { handleExpress } = require('../utils/handle-express')

const router = require('express').Router()

router.get('/', (req, res) => {
    // #swagger.tags = ['Rights']
    handleExpress(res, [
        {name: "superadmin", title: 'Super admin admin of whole system'},
        {name: "admin", title: 'Simple service admin'},
        {name: "user.r", title: 'read users'},
        {name: "user.w", title: 'write users'},
    ])
})

router.post('/', (req, res) => {
    // #swagger.tags = ['Services']
    const { name, rightsUrl } = req.body
    handleExpress(async () => {
        const db = await connect()
        const { Services } = db.models

        const service = await Services.create({name, rightsUrl})
        await service.updateRights()
        return 
    })
})

router.get('/:id', (req, res) => {
    // #swagger.tags = ['Services']
    handleExpress(res, async () => {
        const db = await connect()
        const User = require('../models/user')(db)
        return await User.findAll()
    })
})

router.put('/:id', (req, res) => {
    // #swagger.tags = ['Services']
    res.send('asdasd')
})

router.delete('/:id', (req, res) => {
    // #swagger.tags = ['Services']
    res.send('asdasd')
})


module.exports = router