const { connect } = require('../services/db')
const { handleExpress } = require('../utils/handle-express')
const { auth } = require('../services/acl')

const router = require('express').Router()

router.get('/', (req, res) => {
    // #swagger.tags = ['Sessions']
    handleExpress(res, async () => {
        const db = await connect()
        const { Sessions } = db.models
        return Sessions.findAll({ include: 'User' })
    })
})

router.post('/', (req, res, next) => {
    const { login, password } = req.body
    req.headers.authorization = 'Basic ' + Buffer.from(login + ':' + password).toString('base64')
    next()
}, auth, (req, res) => {

    // #swagger.tags = ['Sessions']
    handleExpress(res, async () => {
        const db = await connect()
        const { Sessions } = db.models

        // TODO: add location with https://www.ip2location.io/

        const token = req.user.buildToken()

        await Sessions.create({
            UserId: req.user.id,
            token,
            userAgent: req.headers.userAgent,
            userAddress: req.socket.remoteAddress
        })
        return token
    })
})

router.delete('/:id', auth, (req, res) => {
    // #swagger.tags = ['Sessions']
    handleExpress(res, req.session.destroy())
})

module.exports = router