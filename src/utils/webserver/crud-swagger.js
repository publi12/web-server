require('dotenv').config()
const express = require('express')
const swaggerAutogen = require('swagger-autogen')({ writeOutputFile: false });
const cors = require('cors')
const fs = require('fs')

const app = express()
const swaggerUi = require('swagger-ui-express');
const path = require('path');
const { readConfig } = require('./crud-models');
const { expressCrud, crudRouter } = require('../../services/acl');
const { cap } = require('./cap');

function buildModelSchema(model) {
    const field = f => {
        if (f.type == 'password')
            return { [f.name]: { type: 'string' } }

        if (f.type == 'json')
            return { [f.name]: { type: 'object' } }

        if (f.isRef)
            return { [cap(f.name, 'Id')]: { type: 'number' } }

        return { [f.name]: { type: f.type } }
    }

    return {
        type: 'object',
        properties: model.fields
            .filter(x => !x.isArray)
            .reduce((s, c) => ({
                ...s,
                ...field(c)
            }), {})
    }
}

function enrichSwaggerWithCrud(swagger, crudModels){
    const baseUrl = '/api/crud/'
    swagger.components = swagger.components || {}
    swagger.components.schemas = swagger.components.schemas || {}

    for (let model of crudModels) {
        const title = model.name.slice(0, 1).toUpperCase() + model.name.slice(1)
        const tags = [title]

        swagger.components.schemas[model.name] = buildModelSchema(model)
        const schema = { $ref: `#/components/schemas/${model.name}` }

        swagger.paths[`${baseUrl}${model.name}s`] = {
            get: {
                tags,
                parameters: [
                    { name: 'fields', in: 'query' },
                    { name: 'filter', in: 'query' },
                    { name: 'skip', in: 'query' },
                    { name: 'limit', in: 'query' },
                    { name: 'order', in: 'query' }
                ],
                responses: { 200: { description: 'desc', schema: { type: 'array', items: { allOf: [schema] } } } },
            },

            post: {
                tags,
                parameters: [
                    { name: 'body', in: 'body', schema }
                ],
                responses: { default: { description: '', schema } },
            }
        }

        swagger.paths[`${baseUrl}${model.name}s/{id}`] = {
            get: {
                tags,
                parameters: [
                    { name: 'id', in: 'path', required: true, type: 'string' },
                ],
                responses: { default: { description: '', schema } },
            },

            put: {
                tags,
                parameters: [
                    { name: 'id', in: 'path', required: true, type: 'string' },
                    { name: 'body', in: 'body', schema }
                ],
                responses: { default: { description: '', schema } },
            },

            delete: {
                tags,
                parameters: [
                    { name: 'id', in: 'path', required: true, type: 'string' },
                ],
                responses: { default: { description: '' } },
            }
        }

        // for (let sub of model.subs) {
        //     console.log(sub)
        //     const refSchema = { $ref: `#/components/schemas/${sub.type}` }
        //     data.paths[`/api/${model.name}s/{id}/${sub.name}`] = {
        //         get: {
        //             tags,
        //             description: 'Get all attached items',
        //             parameters: [
        //                 { name: 'id', in: 'path', required: true, type: 'string' },
        //             ],
        //             responses: { 200: { description: 'desc', schema: { type: 'array', items: { allOf: [refSchema] } } } },
        //         },

        //         post: {
        //             tags,
        //             description: 'Create and attached item',
        //             parameters: [
        //                 { name: 'id', in: 'path', required: true, type: 'string' },
        //                 { name: 'body', in: 'body', schema: refSchema }
        //             ],
        //             responses: { default: { description: '', schema: refSchema } },
        //         },

        //         put: {
        //             tags,
        //             description: 'Set attached items',
        //             parameters: [
        //                 { name: 'id', in: 'path', required: true, type: 'string' },
        //                 { name: 'body', in: 'body', schema: { type: 'array', items: { allOf: [{ type: 'integer' }] } } }
        //             ],
        //             responses: { default: { description: '', schema: refSchema } },
        //         },

        //         delete: {
        //             tags,
        //             description: 'Detach all items',
        //             parameters: [
        //                 { name: 'id', in: 'path', required: true, type: 'string' },
        //             ],
        //             responses: { default: { description: '' } },
        //         },

        //     }

        //     data.paths[`/api/${model.name}s/{id}/${sub.name}/{sid}`] = {
        //         delete: {
        //             tags,
        //             description: 'Detach single item',
        //             parameters: [
        //                 { name: 'id', in: 'path', required: true, type: 'string' },
        //                 { name: 'sid', in: 'path', required: true, type: 'string' },
        //             ],
        //             responses: { default: { description: '' } },
        //         }
        //     }
        // }
    }

    // app.use('/api/crud', (req, res, next) => { expressCrud(req, res, next) })
    // app.use('/api/crud/', crudRouter)
}

module.exports = {
    enrichSwaggerWithCrud
}