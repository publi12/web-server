const { updateCrudModels } = require("./crud-models")
const { connect } = require("../../services/db")




/**
 * 
 * @param {string} modelsPath path to yml config file of models
 * @param {bool} enableAcl 
 * @returns 
 */
async function installWebServer(modelsPath) {
    // let updated = 
    updateCrudModels(modelsPath)
    // console.log(updated)
    let db = await connect(true)
    // try {
    //     await db.query('SELECT * FROM users LIMIT 1', { raw: true, plain: true })
    // }
    // catch {
    //     updated = true
    // }
    
    // db = await connect(updated)
    // console.log({updated})    

    const { Users, Services, Roles, Permissions } = db.models
    const cnt = await Users.count()

    // console.log(cnt)

    // First run.
    if (cnt == 0) {
        console.log('First run. Creating default acl...')
        const { ADMIN_USER, ADMIN_PASS } = process.env
        if (!ADMIN_USER || !ADMIN_PASS) throw 'ADMIN_USER or ADMIN_PASS not defined'

        // Create base service.
        const svc = await Services.create({ name: 'passport' })
        // console.log(svc)

        // Create base permission.
        const perm = await svc.createPermission({ path: "*", title: "all" })

        // Create base role.
        const role = await Roles.create({ name: "sa" })
        await role.addPermission(perm)

        // If admin defined. Create base.
        if (ADMIN_USER && ADMIN_PASS) {
            const user = await Users.create({ login: ADMIN_USER, password: ADMIN_PASS })
            await user.addRole(role)
        }
        console.log('Default acl created.')
    }

    // console.log({ cnt })


    // if (login == process.env.ADMIN_USER && password == process.env.ADMIN_PASS) {
    //     console.log('Superadmin')
    //     user = new Users({ id: 0 })
    // }

}

module.exports = { installWebServer }