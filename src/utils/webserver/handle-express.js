async function handleExpress(res, action) {
    try {
        if (action instanceof Function) action = action()
        const result = action.then ? (await action) : action
    
        if (res.headersSent) return
        if (!result) return res.send({})        

        res.send(typeof(result) == 'object' || result.constructor === Object || result.constructor === Array ? result : { data: result })
    }
    catch (ex) {
        const error = ex && ex.message || ex
        const pm = ex.parent && ex.parent.sqlMessage
        const em = ex.errors && ex.errors.map(e => e.message).join('; ')

        if (isFinite(ex)) return res.sendStatus(ex)
        res.status(400).send({ message: pm || em || error })
    }
}

module.exports = { handleExpress }