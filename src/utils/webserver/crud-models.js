const fs = require('fs');
const path = require('path');

const capitalize = x => x[0].toUpperCase() + x.slice(1)
const toTablename = x => x[0].toUpperCase() + (x.endsWith('s') ? x.slice(1) : (x.slice(1) + 's'))

const typeHash = {
    'integer': 'DataTypes.INTEGER',
    'float': 'DataTypes.FLOAT',
    'string': 'DataTypes.STRING',
    'text': 'DataTypes.TEXT',
    'image': 'DataTypes.BLOB',
    'blob': 'DataTypes.BLOB',
    'json': 'DataTypes.JSON',
    'date': 'DataTypes.DATE',
    'password': 'DataTypes.STRING',
}

function getModelsDir(configPath) {
    const fname = path.basename(configPath, '.yml')
    const modelsDir = path.join(path.dirname(configPath), fname)
    if (!fs.existsSync(modelsDir)) fs.mkdirSync(modelsDir)
    return modelsDir
}

function readConfig(filepath) {
    filepath = path.resolve(filepath)

    // Читаем содержимое файла
    const lines = fs.readFileSync(filepath).toString().split('\n');

    const re = /([ \t]*)([\W\w]+)[\r]?/

    const tables = []
    let curTable = null

    for (let line of lines) {
        const r = re.exec(line)
        if (!r) continue;
        if (line.trim().slice(0, 1) == '#') continue;

        const [_, tabs, value] = r

        let [name2, type2] = (value || '').split(':').map(x => x.trim())

        if (!name2) continue;
        type2 = type2 || name2

        name2 = name2.startsWith('@') ? name2.slice(1) : name2

        // table name
        if (tabs.length == 0) {
            curTable = { name: name2, fields: [] }
            tables.push(curTable)
        }
        // fields section
        else {
            const isRequired = type2 && type2.includes('*')
            const isUnique = type2 && type2.includes('!')
            const isArray = type2 && (type2.includes('[]') || type2.endsWith('s'))
            const isRef = type2.startsWith('@')
            const name = name2 //isRef ? name2.slice(1) : name2
            const singleName = name.endsWith('s') ? name.slice(0, -1) : name

            const type = type2 ? type2.replace(/[@\*\!\[\]]/g, '') : singleName
            const singleType = type.endsWith('s') ? type.slice(0, -1) : type
            const isSimple = !!typeHash[singleType]

            curTable.fields.push({ name, type: singleType, isSimple, isRequired, isUnique, isArray, isRef })
        }
    }

    tables.forEach(x => enrichSubs(x, tables))

    return tables
}

function enrichSubs(model, models) {
    let subs = []

    const refs = model.fields.filter(x => x.isArray)
    refs.forEach(x => subs.push({ name: x.name, type: x.type }))

    const refModels = models.filter(x => x.fields.filter(y => y.type == model.name).length)
    refModels.forEach(x => subs.push({ name: x.name + 's', type: x.name }))

    model.subs = subs
}

function writeConfig(tables) {

}

function buildModel(table) {
    const cName = capitalize(table.name) + 's'

    const tpl = fs.readFileSync(__dirname + '/model.tpl.js').toString()

    const associations = []
    const fields = []

    const indexes = []
    const exclude = []
    const methods = ['']
    const hooks = []

    for (let field of table.fields) {
        const cType = capitalize(field.type) + 's'

        const type = typeHash[field.type]

        if (type) {
            // if (!type) throw `Field \x1b[34m${table.name}.${field.name}\x1b[0m has unknown type \x1b[34m${field.type}\x1b[0m`

            const tt = field.isArray ? `DataTypes.ARRAY(${type})` : type
            let fcode = `${field.name}: {type: ${tt}`

            if (field.isUnique) indexes.push(`{ unique: true, fields: ["${field.name}"] }`)
            if (field.isRequired) fcode += ', allowNull: false'


            if (field.name == 'password') {
                fcode += `, get(){}, set(value) {this.setDataValue('password', bcrypt.hashSync(value, bcrypt.genSaltSync(10)) )}`

                methods.push(`
        static async login(login, password){
            const where = sequelize.where(sequelize.fn('LOWER', sequelize.col('login')), login.toLowerCase())
            const user = await this.findOne({ where });
            return (user && bcrypt.compareSync(password, user.getDataValue('password'))) ? user : null;
        }`)
            }

            fields.push(`${fcode} }`)
        }
        else {
            if (field.isRef) {
                associations.push(`models.${cType}.hasOne(models.${cName}, {onDelete: 'cascade'})`)
                associations.push(`models.${cName}.belongsTo(models.${cType}, {onDelete: 'cascade'})`)
            }
            else if (!field.isArray) {
                associations.push(`models.${cName}.hasOne(models.${cType}, {onDelete: 'cascade'})`)
                associations.push(`models.${cType}.belongsTo(models.${cName}, {onDelete: 'cascade'})`)
            }
            else {
                const single = field.name.endsWith('s') ? field.name.slice(0, -1) : field.name
                const over = `${table.name}_${single}`
                associations.push(`models.${cName}.belongsToMany(models.${cType}, {through: '${over}', onDelete: 'cascade'})`)
                associations.push(`models.${cType}.belongsToMany(models.${cName}, {through: '${over}', onDelete: 'cascade'})`)
            }
        }
    }

    let code = tpl.replace('$TABLE', cName)
    code = code.replace('$FIELDS', fields.join(',\r\n\t\t\t'))
    code = code.replace('$ASSOCIATIONS', associations.join('\r\n\t\t\t'))
    code = code.replace('$INDEXES', indexes.join('\r\n\t\t\t'))
    code = code.replace('$EXCLUDE', JSON.stringify(exclude))
    code = code.replace('$METHODS', methods.join('\r\n\r\n\t\t'))
    code = code.replace('$HOOKS', hooks.join(',\r\n\t\t\t\t'))

    return code
}

function updateModels(tables, outDir) {
    if (!fs.existsSync(outDir)) fs.mkdirSync(outDir, { recursive: true })

    let updated = false

    for (let table of tables) {
        const code = buildModel(table)

        const filename = path.join(outDir, `/${table.name.toLowerCase()}.js`)
        if (fs.existsSync(filename)) {
            const old = fs.readFileSync(filename).toString()
            if (old == code) continue;
        }
        updated = true
        fs.writeFileSync(filename, code)
        console.log(`Crud update model \x1b[34m${table.name}\x1b[0m`)
    }

    // Remove old models
    const names = tables.map(x => x.name + '.js')
    const files = fs.readdirSync(outDir)
    const drop = files.filter(x => !names.includes(x))
    drop.map(x => {
        updated = true
        fs.unlinkSync(path.join(outDir, x))
        console.log(`Crud remove model \x1b[31m${x}\x1b[0m`)
    })

    return updated
}

function updateCrudModels(configPath) {
    configPath = path.resolve(configPath)
    const modelsDir = getModelsDir(configPath)

    const tables = readConfig(configPath)
    return updateModels(tables, modelsDir)
}

module.exports = { readConfig, updateCrudModels, toTablename }