require('dotenv').config()
const express = require('express')
const swaggerAutogen = require('swagger-autogen')({ writeOutputFile: false });
const cors = require('cors')
const fs = require('fs')

const app = express()
const swaggerUi = require('swagger-ui-express');
const path = require('path');
const { readConfig } = require('./crud-models');
const { expressCrud, crudRouter } = require('../../services/acl');
const { cap } = require('../cap');
const { buildCrudSwagger } = require('./crud-swagger');

function buildModelSchema(model) {
    const field = f => {
        if (f.type == 'password')
            return { [f.name]: { type: 'string' } }

        if (f.type == 'json')
            return { [f.name]: { type: 'object' } }

        if (f.isRef)
            return { [cap(f.name, 'Id')]: { type: 'number' } }

        return { [f.name]: { type: f.type } }
    }

    return {
        type: 'object',
        properties: model.fields
            .filter(x => !x.isArray)
            .reduce((s, c) => ({
                ...s,
                ...field(c)
            }), {})
    }
}



function serve() {
    console.log(`Building swagger...`)
    const swaggerFile = __dirname + '/swagger.json';
    const startRouterPath = path.resolve(__dirname + '/../routes/index.js')
    const routes = [startRouterPath, './routes/index.js'];

    const swaggerBase = {
        securityDefinitions: {
            BasicAuth: { type: 'basic' },
            apiKeyAuth: {
                type: 'apiKey',
                in: 'header', // can be 'header', 'query' or 'cookie'
                name: 'Authorization', // name of the header, query parameter or cookie
            }
        },
        security: [{ BasicAuth: [], apiKeyAuth: [] }],
        schemes: ["http", "https"]
    }

    return swaggerAutogen(swaggerFile, routes, swaggerBase)
        .then(async ({ data }) => {
            data.host = ''

            // app.swaggerRouter = express.Router()
            // app.swaggerRouter.use(swaggerUi.serve, swaggerUi.setup(data))

            app.use(cors())
            app.use(express.json({ limit: '50mb' }))
            app.use(require(startRouterPath))

            const perms = []
            for (let p in data.paths) {
                for (let m in data.paths[p]) {
                    perms.push([m, p])
                }
            }

            const crudModels = readConfig()

            data.components = data.components || {}
            data.components.schemas = data.components.schemas || {}

            const crudBaseUrl = '/api/crud/'

            for (let model of crudModels) {
                const title = model.name.slice(0, 1).toUpperCase() + model.name.slice(1)
                const tags = [title]

                // console.log(model.fields)

                console.log(buildModelSchema(model))

                data.components.schemas[model.name] = buildModelSchema(model)

                const schema = { $ref: `#/components/schemas/${model.name}` }

                data.paths[`${crudBaseUrl}${model.name}s`] = {
                    get: {
                        tags,
                        parameters: [
                            { name: 'fields', in: 'query' },
                            { name: 'filter', in: 'query' },
                            { name: 'skip', in: 'query' },
                            { name: 'limit', in: 'query' },
                            { name: 'order', in: 'query' }
                        ],
                        responses: { 200: { description: 'desc', schema: { type: 'array', items: { allOf: [schema] } } } },
                    },

                    post: {
                        tags,
                        parameters: [
                            { name: 'body', in: 'body', schema }
                        ],
                        responses: { default: { description: '', schema } },
                    }
                }

                data.paths[`${crudBaseUrl}${model.name}s/{id}`] = {
                    get: {
                        tags,
                        parameters: [
                            { name: 'id', in: 'path', required: true, type: 'string' },
                        ],
                        responses: { default: { description: '', schema } },
                    },

                    put: {
                        tags,
                        parameters: [
                            { name: 'id', in: 'path', required: true, type: 'string' },
                            { name: 'body', in: 'body', schema }
                        ],
                        responses: { default: { description: '', schema } },
                    },

                    delete: {
                        tags,
                        parameters: [
                            { name: 'id', in: 'path', required: true, type: 'string' },
                        ],
                        responses: { default: { description: '' } },
                    }
                }

                // for (let sub of model.subs) {
                //     console.log(sub)
                //     const refSchema = { $ref: `#/components/schemas/${sub.type}` }
                //     data.paths[`/api/${model.name}s/{id}/${sub.name}`] = {
                //         get: {
                //             tags,
                //             description: 'Get all attached items',
                //             parameters: [
                //                 { name: 'id', in: 'path', required: true, type: 'string' },
                //             ],
                //             responses: { 200: { description: 'desc', schema: { type: 'array', items: { allOf: [refSchema] } } } },
                //         },

                //         post: {
                //             tags,
                //             description: 'Create and attached item',
                //             parameters: [
                //                 { name: 'id', in: 'path', required: true, type: 'string' },
                //                 { name: 'body', in: 'body', schema: refSchema }
                //             ],
                //             responses: { default: { description: '', schema: refSchema } },
                //         },

                //         put: {
                //             tags,
                //             description: 'Set attached items',
                //             parameters: [
                //                 { name: 'id', in: 'path', required: true, type: 'string' },
                //                 { name: 'body', in: 'body', schema: { type: 'array', items: { allOf: [{ type: 'integer' }] } } }
                //             ],
                //             responses: { default: { description: '', schema: refSchema } },
                //         },

                //         delete: {
                //             tags,
                //             description: 'Detach all items',
                //             parameters: [
                //                 { name: 'id', in: 'path', required: true, type: 'string' },
                //             ],
                //             responses: { default: { description: '' } },
                //         },

                //     }

                //     data.paths[`/api/${model.name}s/{id}/${sub.name}/{sid}`] = {
                //         delete: {
                //             tags,
                //             description: 'Detach single item',
                //             parameters: [
                //                 { name: 'id', in: 'path', required: true, type: 'string' },
                //                 { name: 'sid', in: 'path', required: true, type: 'string' },
                //             ],
                //             responses: { default: { description: '' } },
                //         }
                //     }
                // }
            }

            // app.use('/api/crud', (req, res, next) => { expressCrud(req, res, next) })
            
            // enrich buildCrudSwagger(data, )
            app.use('/api/crud/',  crudRouter)
            app.use('/api', swaggerUi.serve, swaggerUi.setup(data));
            


            // Attach default ui
            app.use(
                express.static(__dirname + '/../routes/ui/'),
                (req, res) => res.sendFile(__dirname + '/../routes/ui/index.html')
            )

            const port = process.env.PORT || 80

            console.log(`Working on port ${port}`)
            app.listen(port);

            return { app, perms }
        });
}

module.exports = serve