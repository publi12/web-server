const { Router } = require("express")
const { connect } = require("../../services/db")
const { toTablename } = require("./crud-models")
const { handleExpress } = require("./handle-express")
const { Sequelize } = require("sequelize")

const crudRouter = Router()

crudRouter.use('/:model', async (req, res, next) => {
    // !!! ACL
    const db = await connect()
    const model = db.models[toTablename(req.params.model)]
    req.db = db
    req.model = model
    next()
})

crudRouter.get('/:model', async (req, res) => {
    let { fields, filter, skip, limit, order } = req.query
    // const filter = req.query && req.query.filter

    const attributes = fields && fields.split(',').filter(x => x)
    offset = skip && parseInt(skip)
    limit = Math.min(Math.max(parseInt(limit) || 1000), 1000)
    order = order && order.split(',').filter(x => x).map(x => x.includes('-') ? [x.replace('-', '').trim(), 'DESC'] : x)
   
    console.log(order)

    const sanitized_input = (filter || '').split(';')[0]
    const where = Sequelize.literal(sanitized_input)

    handleExpress(res, req.model.findAll({ attributes, offset, limit, order, where }))    
})

crudRouter.post('/:model', async (req, res) => {
    handleExpress(res, req.model[Array.isArray(req.body) ? 'bulkCreate' : 'create'](req.body))
})

crudRouter.get('/:model/:id', async (req, res) => {
    handleExpress(res, req.model.findByPk(req.params.id))
})

crudRouter.put('/:model/:id', async (req, res) => {
    handleExpress(res, req.model.update(req.body, { where: { id: req.params.id } }))
})

crudRouter.delete('/:model/:id', async (req, res) => {
    handleExpress(res, req.model.destroy({ where: { id: req.params.id } }))
})

module.exports = crudRouter