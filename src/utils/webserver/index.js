require('dotenv').config()
const express = require('express')
const swaggerAutogen = require('swagger-autogen')({ writeOutputFile: false });
const cors = require('cors')

const swaggerUi = require('swagger-ui-express');
const path = require('path');
const { enrichSwaggerWithCrud } = require('./crud-swagger');
const { readConfig } = require('./crud-models');
const crudRouter = require('./crud-router');
const { installWebServer } = require('./install');

function createSwagger(routerPath) {
    routerPath = path.resolve(routerPath)

    console.log(`Building swagger...`)
    const swaggerFile = __dirname + '/swagger.json';
    const startRouterPath = routerPath //path.resolve(__dirname + '/../routes/index.js')
    const routes = [startRouterPath];

    const swaggerBase = {
        host: '',
        securityDefinitions: {
            BasicAuth: { type: 'basic' },
            apiKeyAuth: {
                type: 'apiKey',
                in: 'header', // can be 'header', 'query' or 'cookie'
                name: 'Authorization', // name of the header, query parameter or cookie
            }
        },
        security: [{ BasicAuth: [], apiKeyAuth: [] }],
        schemes: ["http", "https"]
    }

    return swaggerAutogen(swaggerFile, routes, swaggerBase).then(x => x.data)
}



async function createWebServer(routerPath, modelsPath) {
    await installWebServer(modelsPath)

    const swagger = await createSwagger(routerPath)

    const app = express()
    app.use(cors())
    app.use(express.json({ limit: '50mb' }))
    app.use(require(routerPath))

    if (modelsPath) {
        const crudModels = readConfig(modelsPath)
        enrichSwaggerWithCrud(swagger, crudModels)
        app.use('/api/crud/', crudRouter)
    }

    app.use('/api', swaggerUi.serve, swaggerUi.setup(swagger));

    // --- Extracting all pathes (use swagger instead)
    // const perms = []
    // for (let p in data.paths) {
    //     for (let m in data.paths[p]) {
    //         perms.push([m, p])
    //     }
    // }

    // --- System frontend
    //     app.use(
    //         express.static(__dirname + '/../routes/ui/'),
    //         (req, res) => res.sendFile(__dirname + '/../routes/ui/index.html')
    //     )


    const port = process.env.PORT || 80
    console.log(`Working on port ${port}`)
    app.listen(port);

    return { app }


    return 100
}

module.exports = {
    createWebServer
}