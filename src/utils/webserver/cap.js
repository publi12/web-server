/**
 *
 * @param {string} x
 * @returns
 */
const cap = (s, suf) => s.slice(0, 1).toUpperCase() + s.slice(1) + (suf || '');
exports.cap = cap;
