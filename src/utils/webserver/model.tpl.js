"use strict";
const { Model, DataTypes } = require("sequelize");
const bcrypt = require("bcrypt");

module.exports = (sequelize) => {
	class MyModel extends Model {
		/**
		 * Helper method for defining associations.
		 * This method is not a part of Sequelize lifecycle.
		 * The `models/index` file will call this method automatically.
		 */
		static associate(models) {
			// define association here
			$ASSOCIATIONS
		}$METHODS
	}

	MyModel.init(
		{
			$FIELDS
		},
		{
			sequelize,
			modelName: "$TABLE",
			indexes: [
				$INDEXES
			],
			defaultScope: {
				attributes: { exclude: $EXCLUDE },
			},
			scopes: {
				withPassword: { attributes: {} }
			},
			hooks:{
				$HOOKS
			}
		}
	);

	return MyModel;
};
