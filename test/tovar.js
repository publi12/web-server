"use strict";
const { Model, DataTypes } = require("sequelize");

module.exports = (sequelize) => {
	class MyModel extends Model {
		/**
		 * Helper method for defining associations.
		 * This method is not a part of Sequelize lifecycle.
		 * The `models/index` file will call this method automatically.
		 */
		static associate(models) {
			// define association here
			models.tovar.hasOne(models.number)
			models.number.belongsTo(models.tovar)
		}
	}

	MyModel.init(
		{
			name: DataType.STRING
		},
		{
			sequelize,
			modelName: "tovars"
		}
	);

	return MyModel;
};
