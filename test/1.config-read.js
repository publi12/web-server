const fs = require('fs');
const path = require('path');
const walkdir = require('walkdir');

function parseConfig(filename) {
    // Читаем содержимое файла
    const lines = fs.readFileSync(filename).toString().split('\n');

    const re = /([ \t]*)([\W\w]+)[\r]?/

    const tables = []
    let curTable = null

    const simpleTypes = 'date,integer,float,string,text,image,blob'.split(',')

    for (let line of lines) {
        const r = re.exec(line)
        if (!r) continue;
        if (line.trim().slice(0, 1) == '#') continue;

        const [_, tabs, value] = r

        const [name2, type2] = (value || '').split(':').map(x => x.trim())

        if (!name2) continue;

        // table name
        if (tabs.length == 0) {
            curTable = { name: name2, fields: [] }
            tables.push(curTable)
        }
        // fields section
        else {
            const isRequired = type2 && type2.includes('*')
            const isUnique = type2 && type2.includes('!')
            const isArray = type2 ? type2.includes('[]') : name2.endsWith('s')
            const isRef = name2.startsWith('@')
            const name = isRef ? name2.slice(1) : name2
            const singleName = name.endsWith('s') ? name.slice(0, -1) : name
            const type = type2 ? type2.replace(/[\*\!\[\]]/g, '') : singleName
            const isSimple = simpleTypes.includes(type)

            curTable.fields.push({ name, type, isRequired, isUnique, isArray, isSimple, isRef })
        }
    }

    return tables
}

const capitalize = x => x[0].toUpperCase() + x.slice(1)

function buildTableModel(table) {

    const cName = capitalize(table.name) + 's'
 
    const tpl = fs.readFileSync(__dirname + '/model.tpl.js').toString()

    const associations = []
    const fields = []

    const typeHash = {
        'integer': 'DataTypes.INTEGER',
        'float': 'DataTypes.FLOAT',
        'string': 'DataTypes.STRING',
        'text': 'DataTypes.TEXT',
        'image': 'DataTypes.BLOB',
        'blob': 'DataTypes.BLOB',
        'json': 'DataTypes.JSON',
        'date': 'DataTypes.DATE',
    }

    for (let field of table.fields) {
        const cType = capitalize(field.type) + 's'

        if (field.isSimple) {
            const type = typeHash[field.type]
            if (!type) throw `Field \x1b[34m${table.name}.${field.name}\x1b[0m has unknown type \x1b[34m${field.type}\x1b[0m`
            let fcode = `${field.name}: {type: ${type}`
            if (field.isUnique) fcode += ', unique: true'
            if (field.isRequired) fcode += ', allowNull: false'
            fields.push(`${fcode} }`)
        }
        else {
            if (field.isRef) {
                associations.push(`models.${cType}.hasOne(models.${cName})`)
                associations.push(`models.${cName}.belongsTo(models.${cType})`)
            }
            else if (!field.isArray) {
                associations.push(`models.${cName}.hasOne(models.${cType})`)
                associations.push(`models.${cType}.belongsTo(models.${cName})`)
            }
            else {
                const over = `${field.type}_${table.name}`
                associations.push(`models.${cName}.belongsToMany(models.${cType}, {through: '${over}'})`)
                associations.push(`models.${cType}.belongsToMany(models.${cName}, {through: '${over}'})`)
            }
        }
    }

    let code = tpl.replace('$TABLE', cName)
    code = code.replace('$FIELDS', fields.join(',\r\n\t\t\t'))
    code = code.replace('$ASSOCIATIONS', associations.join('\r\n\t\t\t'))

    return code
}

function updateModels(tables, outDir) {
    if (!fs.existsSync(outDir)) fs.mkdirSync(outDir, { recursive: true })

    for (let table of tables) {
        const code = buildTableModel(table)

        const filename = path.join(outDir, `/${table.name.toLowerCase()}.js`)
        if (fs.existsSync(filename)) {
            const old = fs.readFileSync(filename).toString()
            if (old == code) continue;
        }
        fs.writeFileSync(filename, code)
    }

    const names = tables.map(x => x.name + '.js')
    const files = fs.readdirSync(outDir)
    const drop = files.filter(x => !names.includes(x))
    drop.map(x => fs.unlinkSync(path.join(outDir, x)))

    // console.log(drop)
    // Remove old models

}

const tables = parseConfig(__dirname + '/../src/models/crud.yml')

updateModels(tables, __dirname + '/../src/models/crud')

// console.log(buildTableModel(tables[0]))
// console.log(buildTableModel(tables[1]))
// console.log(buildTableModel(tables[2]))


// data = lines

// Парсим YAML
// const data = yaml.parse(content);

// Выводим данные на консоль
// console.log(data);


walkdir(__dirname + '/../src/models', x => console.log(path.basename(x)))