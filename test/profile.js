"use strict";
const { Model, DataTypes } = require("sequelize");

module.exports = (sequelize) => {
	class MyModel extends Model {
		/**
		 * Helper method for defining associations.
		 * This method is not a part of Sequelize lifecycle.
		 * The `models/index` file will call this method automatically.
		 */
		static associate(models) {
			// define association here
			models.user.hasOne(models.profile)
			models.profile.belongsTo(models.user)
		}
	}

	MyModel.init(
		{
			photo: DataType.BLOB
		},
		{
			sequelize,
			modelName: "profiles"
		}
	);

	return MyModel;
};
