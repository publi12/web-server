"use strict";
const { Model, DataTypes } = require("sequelize");

module.exports = (sequelize) => {
	class MyModel extends Model {
		/**
		 * Helper method for defining associations.
		 * This method is not a part of Sequelize lifecycle.
		 * The `models/index` file will call this method automatically.
		 */
		static associate(models) {
			// define association here
			models.user.hasMany(models.tovar, {through: 'tovar_user'})
			models.tovar.belongsToMany(models.user, {through: 'tovar_user'})
		}
	}

	MyModel.init(
		{
			name: DataType.STRING,
			age: DataType.INTEGER
		},
		{
			sequelize,
			modelName: "users"
		}
	);

	return MyModel;
};
