
process.env.DB_URI = 'sqlite:///:memory:'

const { default: axios } = require("axios");
const { connect } = require("../src/services/db");
const serve = require("../src/utils/webserver");

let server
// const db = await connect(true)


test('startup', async () => {
    await connect(true)
    await serve().then(x => server = x)
});

test('No users at start', async () => {
    const users = await fetch('http://localhost/api/users').then(x => x.json())
    expect(users.data).toEqual([]);
});

test('Create user', async () => {
    const res = await axios.post('http://localhost/api/users',
        { login: 'login', password: 'password' })
    // const users = await fetch('http://localhost/api/users').then(x => x.json())
    // expect(users.data). toEqual([]);
    console.log(res)
});


test('shutdown', async () => {
    server.close()
});